#include "boss.h"
#include "fire.h"
#include "game.h"
#include <QGraphicsScene>
#include <QTimer>
#include <QDebug>
#include <QList>
#include "player.h"

extern Game * game;


Boss::Boss(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent),health(3)
{
    setPixmap(QPixmap(":/images/boss.png"));
    int random_number = rand() % 400;
    setPos(800, random_number);

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    QTimer *timer2 = new QTimer();
    connect(timer2, SIGNAL(timeout()), this, SLOT(fire()));

    timer -> start(200);
    timer2 -> start(8000);


}

void Boss::move()
{
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i=0, n = colliding_items.size(); i < n; i++)
    {
        if (typeid(*(colliding_items[i])) == typeid(Player))
        {
            game->health->decrease();
            game->health->decrease();
            scene() -> removeItem(this);
            delete this;
            return;
        }


        if (typeid(*(colliding_items[i])) == typeid(Bullet))
        {
            this->decrease();
            scene() -> removeItem(colliding_items[i]);
            delete colliding_items[i];
            if (this->getHealth() == 0)
            {
                scene() -> removeItem(this);
                delete this;
                return;
            }
            return;
        }

    }
    setPos(x()-5, y());
    if (pos().x() < -20)
    {
        scene() -> removeItem(this);
        delete this;
    }
}


void Boss::fire()
{
    Fire *fire = new Fire();
    fire->setPos(x(), y());
    scene() -> addItem(fire);
}



void Boss::decrease()
{
    health--;
}

int Boss::getHealth()
{
    return health;
}
