#ifndef BOSS_H
#define BOSS_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>


class Boss : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Boss(QGraphicsItem *parent = 0);
    void decrease();
    int getHealth();

public slots:
    void move();
    void fire();

private:
    int health;



};








#endif // BOSS_H
