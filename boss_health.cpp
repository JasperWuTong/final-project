#include "boss_health.h"


Health::Health()
{
    // initialize the score to 0
    health = 3;
}

void Health::decrease(){
    health--;
}

int Health::getHealth(){
    return health;
}
