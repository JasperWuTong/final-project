#include "enemy.h"
#include "player.h"
#include <QGraphicsScene>
#include <QList>
#include "game.h"
#include <QTimer>

extern Game * game;


Enemy::Enemy(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/images/ghost.png"));

    int random_number = rand() % 400;
    setPos(800, random_number);

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    timer -> start(50);


}

void Enemy::move()
{
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i=0, n = colliding_items.size(); i < n; i++)
    {
        if (typeid(*(colliding_items[i])) == typeid(Player))
        {
            game->health->decrease();
            scene() -> removeItem(this);
            delete this;
            return;
        }
    }


    setPos(x()-5, y());
    if (pos().x() < -20)
    {
        scene() -> removeItem(this);
        delete this;
    }
}

