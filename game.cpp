#include "game.h"
#include <QBrush>
#include <QImage>
#include <QTimer>
#include <QElapsedTimer>
#include <QApplication>
#include <QGraphicsTextItem>
#include <QFont>

Game::Game(QWidget *parent)
{
    scene = new QGraphicsScene();
    setBackgroundBrush(QBrush(QImage(":/images/jungle.jpg")));


    player = new Player();
    scene = new QGraphicsScene();

    player -> setFlag(QGraphicsItem::ItemIsFocusable);
    player -> setFocus();

    scene -> addItem(player);

    QGraphicsView *view = new QGraphicsView(scene);

    player -> setPos(150, view->height()-150);

    view -> show();
    view -> setFixedSize(800,600);
    scene -> setSceneRect(0,0,800,600);


    //spawn_enemy
    QTimer *timer = new QTimer();
    QObject::connect(timer, SIGNAL(timeout()), player, SLOT(spawn()));
    timer -> start(5000);

    //spawn_boss
    QTimer *timer2 = new QTimer();
    QObject::connect(timer2, SIGNAL(timeout()), player, SLOT(spawn_boss()));
    timer2 -> start(8000);

    //spawn_health_package
    QTimer *timer3 = new QTimer();
    QObject::connect(timer3, SIGNAL(timeout()), player, SLOT(spawn_health_package()));
    timer3 -> start(14000);

    //spawn_ammo_package
    QTimer *timer4 = new QTimer();
    QObject::connect(timer4, SIGNAL(timeout()), player, SLOT(spawn_ammo_package()));
    timer4 -> start(18000);

    //remove health package
    QElapsedTimer *elapsedtimer = new QElapsedTimer();
    elapsedtimer->start();
    //QObject::connect(elapsedtimer, SIGNAL(timeout()),  )

    // show ammo
    ammo = new Ammo();
    ammo -> setPos(ammo->x(),ammo->y() + 30);
    scene -> addItem(ammo);

    //show health bar
    health = new Health();
    health -> setPos(health->x(), health->y() + 5);
    scene -> addItem(health);



    // check dead
    connect(health, SIGNAL(dead()),this,SLOT(end()));

    show();
}

void Game::end()
{
    QApplication::quit();
}
