#ifndef PLAYER_H
#define PLAYER_H
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QKeyEvent>

class Player: public QObject, public  QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Player(QGraphicsItem *parent = 0);
    void keyPressEvent(QKeyEvent *event);
public slots:
    void spawn();
    void spawn_boss();
    void spawn_health_package();
    void spawn_ammo_package();
};


#endif // PLAYER_H
