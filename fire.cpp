#include "fire.h"
#include "bullet.h"
#include <QGraphicsScene>
#include <QTimer>
#include <QDebug>
#include <QList>
#include "enemy.h"
#include "boss.h"
#include "game.h"

extern Game * game;


Fire::Fire(QGraphicsItem* parent) : QObject(), QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/images/boss_fire.png"));

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    timer -> start(40);
}

void Fire::move()
{
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i=0, n = colliding_items.size(); i < n; i++)
    {
        if (typeid(*(colliding_items[i])) == typeid(Player))
        {
            game->health->decrease();
            game->health->decrease();

            scene() -> removeItem(this);
            delete this;
            return;
        }



    }


    setPos(x()-10, y());


    if (pos().x() < -20)
    {
        scene() -> removeItem(this);
        qDebug() << "deleted";
        delete this;
    }


}
