#ifndef AMMO_H
#define AMMO_H

#include <QGraphicsTextItem>
#include <QObject>

class Ammo: public QGraphicsTextItem
{
    Q_OBJECT
public:
    Ammo(QGraphicsItem * parent = 0);
    void decrease();
    void increase();
    int getAmmo();
private:
    int ammo;
};




#endif // AMMO_H
