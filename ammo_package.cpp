#include "enemy.h"
#include "healthpackage.h"
#include "player.h"
#include <QGraphicsScene>
#include "game.h"
#include "ammo_package.h"
#include <QElapsedTimer>

extern Game * game;


AmmoPackage::AmmoPackage(QGraphicsItem *parent): QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/images/ammo_package.png"));

    int random_number_x = rand() % 600 + 200;
    int random_number_y = rand() % 400 + 100;
    setPos(random_number_x, random_number_y);

    //QElapsedTimer *elapsedtimer = new QElapsedTimer();
    //elapsedtimer->start();

}
