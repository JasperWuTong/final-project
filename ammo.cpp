#include "ammo.h"
#include <QFont>

Ammo::Ammo(QGraphicsItem* parent) : QGraphicsTextItem(parent)
{
    ammo = 20;

    setPlainText(QString("Ammo: ")+QString::number(ammo));
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times", 16));
}

void Ammo::decrease()
{
    ammo--;
    setPlainText(QString("Ammo: ")+QString::number(ammo));
}

void Ammo::increase()
{
    ammo++;
    setPlainText(QString("Ammo: ")+QString::number(ammo));
}

int Ammo::getAmmo()
{
    return ammo;
}
