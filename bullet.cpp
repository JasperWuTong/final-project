#include "bullet.h"
#include <QGraphicsScene>
#include <QTimer>
#include <QDebug>
#include <QList>
#include "enemy.h"
#include "boss.h"
#include "game.h"

extern Game * game;


Bullet::Bullet(QGraphicsItem* parent) : QObject(), QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/images/bullet.png"));

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    timer -> start(40);
}

void Bullet::move()
{
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i=0, n = colliding_items.size(); i < n; i++)
    {
        if (typeid(*(colliding_items[i])) == typeid(Enemy))
        {
            scene() -> removeItem(colliding_items[i]);
            scene() -> removeItem(this);
            delete colliding_items[i];
            delete this;
            return;
        }
    }


    setPos(x()+10, y());


    if (pos().x() > 820)
    {
        scene() -> removeItem(this);
        qDebug() << "deleted";
        delete this;
    }


}
