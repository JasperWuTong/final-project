#include "player.h"
#include "bullet.h"
#include "game.h"
#include "healthpackage.h"
#include "enemy.h"
#include "health.h"
#include "boss.h"
#include <QDebug>
#include <QList>
#include <QGraphicsScene>
#include "ammo_package.h"

extern Game * game;

Player::Player(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/images/player.png"));
}

void Player::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left)
    {
        QList<QGraphicsItem *> colliding_items = collidingItems();
        for (int i=0, n = colliding_items.size(); i < n; i++)
        {
            if (typeid(*(colliding_items[i])) == typeid(HealthPackage))
            {
                game -> health -> increase();
                game -> health -> increase();
                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
            if (typeid(*(colliding_items[i])) == typeid(AmmoPackage))
            {
                for (int j=0; j<10; j++)
                {
                    game -> ammo -> increase();

                }

                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
        }
        if (pos().x()>0)
        {
            setPos(x()-20, y());
        }
    }
    else if(event->key() == Qt::Key_Right)
    {
        QList<QGraphicsItem *> colliding_items = collidingItems();
        for (int i=0, n = colliding_items.size(); i < n; i++)
        {
            if (typeid(*(colliding_items[i])) == typeid(HealthPackage))
            {
                game -> health -> increase();
                game -> health -> increase();
                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
            if (typeid(*(colliding_items[i])) == typeid(AmmoPackage))
            {
                for (int j=0; j<10; j++)
                {
                    game -> ammo -> increase();

                }

                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
        }
        if (pos().x() + 300 < 800)
          setPos(x()+20, y());
    }
    else if (event->key() == Qt::Key_Up)
    {
        QList<QGraphicsItem *> colliding_items = collidingItems();
        for (int i=0, n = colliding_items.size(); i < n; i++)
        {
            if (typeid(*(colliding_items[i])) == typeid(HealthPackage))
            {
                game -> health -> increase();
                game -> health -> increase();
                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
            if (typeid(*(colliding_items[i])) == typeid(AmmoPackage))
            {
                for (int j=0; j<10; j++)
                {
                    game -> ammo -> increase();

                }

                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
        }
        if (pos().y()>0)
            setPos(x(), y()-20);
    }
    else if (event->key() == Qt::Key_Down)
    {
        QList<QGraphicsItem *> colliding_items = collidingItems();
        for (int i=0, n = colliding_items.size(); i < n; i++)
        {
            if (typeid(*(colliding_items[i])) == typeid(HealthPackage))
            {
                game -> health -> increase();
                game -> health -> increase();
                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
            if (typeid(*(colliding_items[i])) == typeid(AmmoPackage))
            {
                for (int j=0; j<10; j++)
                {
                    game -> ammo -> increase();

                }

                scene() -> removeItem(colliding_items[i]);
                delete colliding_items[i];
                return;
            }
        }
        if (pos().y()<400)
            setPos(x(),y()+20);
    }
    else if(event->key() == Qt::Key_Space)
    {
        if (game->ammo->getAmmo() > 0)
        {
            Bullet *bullet = new Bullet();
            bullet->setPos(x()+75, y()+47);
            scene() -> addItem(bullet);

            game->ammo->decrease();
        }
    }
}

void Player::spawn()
{
    Enemy *enemy = new Enemy();
    scene() -> addItem(enemy);
}

void Player::spawn_boss()
{
    Boss *boss = new Boss();
    scene() -> addItem(boss);
}

void Player::spawn_health_package()
{
    HealthPackage *healthpackage = new HealthPackage();
    scene() -> addItem(healthpackage);
}

void Player::spawn_ammo_package()
{
    AmmoPackage *ammopackage = new AmmoPackage();
    scene() -> addItem(ammopackage);
}









