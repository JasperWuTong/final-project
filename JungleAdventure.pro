#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T14:39:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = JungleAdventure
TEMPLATE = app


SOURCES += main.cpp \
    player.cpp \
    game.cpp \
    bullet.cpp \
    enemy.cpp \
    boss.cpp \
    fire.cpp \
    health.cpp \
    healthpackage.cpp \
    ammo.cpp \
    ammo_package.cpp

HEADERS  += \
    player.h \
    game.h \
    bullet.h \
    enemy.h \
    boss.h \
    fire.h \
    health.h \
    healthpackage.h \
    ammo.h \
    ammo_package.h

FORMS    +=

RESOURCES += \
    res.qrc
