#ifndef GAME_H
#define GAME_H

#include "bullet.h"
#include "player.h"
#include "health.h"
#include "boss.h"
#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QObject>
#include "ammo.h"
#include "ammo_package.h"

class Game: public QGraphicsView
{
    Q_OBJECT
public:
    Game(QWidget * parent = 0);
    QGraphicsScene *scene;
    Player *player;
    Boss *boss;
    Health *health;
    Ammo *ammo;
    AmmoPackage *ammopackage;

public slots:
    void end();

};



#endif // GAME_H
