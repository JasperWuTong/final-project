#ifndef BOSS_HEALTH_H
#define BOSS_HEALTH_H
#include "boss.h"

class Health: public Boss
{
public:
    Health();
    void decrease();
    int getHealth();
private:
    int health;
};




#endif // BOSS_HEALTH_H
