#############################################################################
# Makefile for building: JungleAdventure.app/Contents/MacOS/JungleAdventure
# Generated by qmake (2.01a) (Qt 4.7.4) on: ?? 11? 22 11:08:11 2016
# Project:  JungleAdventure.pro
# Template: app
# Command: /usr/bin/qmake-4.7 -spec /usr/local/Qt4.7/mkspecs/macx-g++ CONFIG+=x86_64 -o Makefile JungleAdventure.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED
CFLAGS        = -pipe -g -gdwarf-2 -arch x86_64 -Xarch_x86_64 -mmacosx-version-min=10.5 -Wall -W $(DEFINES)
CXXFLAGS      = -pipe -g -gdwarf-2 -arch x86_64 -Xarch_x86_64 -mmacosx-version-min=10.5 -Wall -W $(DEFINES)
INCPATH       = -I/usr/local/Qt4.7/mkspecs/macx-g++ -I. -I/Library/Frameworks/QtCore.framework/Versions/4/Headers -I/usr/include/QtCore -I/Library/Frameworks/QtGui.framework/Versions/4/Headers -I/usr/include/QtGui -I/usr/include -I. -F/Library/Frameworks
LINK          = g++
LFLAGS        = -headerpad_max_install_names -arch x86_64 -Xarch_x86_64 -mmacosx-version-min=10.5
LIBS          = $(SUBLIBS) -F/Library/Frameworks -L/Library/Frameworks -framework QtGui -framework QtCore 
AR            = ar cq
RANLIB        = ranlib -s
QMAKE         = /usr/bin/qmake-4.7
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = cp -f
COPY_DIR      = cp -f -R
STRIP         = 
INSTALL_FILE  = $(COPY_FILE)
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = $(COPY_FILE)
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p
export MACOSX_DEPLOYMENT_TARGET = 10.4

####### Output directory

OBJECTS_DIR   = ./

####### Files

SOURCES       = main.cpp \
		player.cpp \
		game.cpp \
		bullet.cpp \
		enemy.cpp \
		boss.cpp \
		fire.cpp \
		health.cpp \
		healthpackage.cpp \
		ammo.cpp \
		ammo_package.cpp moc_player.cpp \
		moc_game.cpp \
		moc_bullet.cpp \
		moc_enemy.cpp \
		moc_boss.cpp \
		moc_fire.cpp \
		moc_health.cpp \
		moc_ammo.cpp \
		qrc_res.cpp
OBJECTS       = main.o \
		player.o \
		game.o \
		bullet.o \
		enemy.o \
		boss.o \
		fire.o \
		health.o \
		healthpackage.o \
		ammo.o \
		ammo_package.o \
		moc_player.o \
		moc_game.o \
		moc_bullet.o \
		moc_enemy.o \
		moc_boss.o \
		moc_fire.o \
		moc_health.o \
		moc_ammo.o \
		qrc_res.o
DIST          = /usr/local/Qt4.7/mkspecs/common/unix.conf \
		/usr/local/Qt4.7/mkspecs/common/mac.conf \
		/usr/local/Qt4.7/mkspecs/common/mac-g++.conf \
		/usr/local/Qt4.7/mkspecs/qconfig.pri \
		/usr/local/Qt4.7/mkspecs/modules/qt_webkit_version.pri \
		/usr/local/Qt4.7/mkspecs/features/qt_functions.prf \
		/usr/local/Qt4.7/mkspecs/features/qt_config.prf \
		/usr/local/Qt4.7/mkspecs/features/exclusive_builds.prf \
		/usr/local/Qt4.7/mkspecs/features/default_pre.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/default_pre.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/dwarf2.prf \
		/usr/local/Qt4.7/mkspecs/features/debug.prf \
		/usr/local/Qt4.7/mkspecs/features/default_post.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/default_post.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/objective_c.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/x86_64.prf \
		/usr/local/Qt4.7/mkspecs/features/warn_on.prf \
		/usr/local/Qt4.7/mkspecs/features/qt.prf \
		/usr/local/Qt4.7/mkspecs/features/unix/thread.prf \
		/usr/local/Qt4.7/mkspecs/features/moc.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/rez.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/sdk.prf \
		/usr/local/Qt4.7/mkspecs/features/resources.prf \
		/usr/local/Qt4.7/mkspecs/features/uic.prf \
		/usr/local/Qt4.7/mkspecs/features/yacc.prf \
		/usr/local/Qt4.7/mkspecs/features/lex.prf \
		/usr/local/Qt4.7/mkspecs/features/include_source_dir.prf \
		JungleAdventure.pro
QMAKE_TARGET  = JungleAdventure
DESTDIR       = 
TARGET        = JungleAdventure.app/Contents/MacOS/JungleAdventure

####### Custom Compiler Variables
QMAKE_COMP_QMAKE_OBJECTIVE_CFLAGS = -pipe \
		-g \
		-gdwarf-2 \
		-arch \
		x86_64 \
		-Xarch_x86_64 \
		-mmacosx-version-min=10.5 \
		-Wall \
		-W


first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: Makefile JungleAdventure.app/Contents/PkgInfo JungleAdventure.app/Contents/Resources/empty.lproj JungleAdventure.app/Contents/Info.plist $(TARGET)

$(TARGET):  $(OBJECTS)  
	@$(CHK_DIR_EXISTS) JungleAdventure.app/Contents/MacOS/ || $(MKDIR) JungleAdventure.app/Contents/MacOS/ 
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

Makefile: JungleAdventure.pro  /usr/local/Qt4.7/mkspecs/macx-g++/qmake.conf /usr/local/Qt4.7/mkspecs/common/unix.conf \
		/usr/local/Qt4.7/mkspecs/common/mac.conf \
		/usr/local/Qt4.7/mkspecs/common/mac-g++.conf \
		/usr/local/Qt4.7/mkspecs/qconfig.pri \
		/usr/local/Qt4.7/mkspecs/modules/qt_webkit_version.pri \
		/usr/local/Qt4.7/mkspecs/features/qt_functions.prf \
		/usr/local/Qt4.7/mkspecs/features/qt_config.prf \
		/usr/local/Qt4.7/mkspecs/features/exclusive_builds.prf \
		/usr/local/Qt4.7/mkspecs/features/default_pre.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/default_pre.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/dwarf2.prf \
		/usr/local/Qt4.7/mkspecs/features/debug.prf \
		/usr/local/Qt4.7/mkspecs/features/default_post.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/default_post.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/objective_c.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/x86_64.prf \
		/usr/local/Qt4.7/mkspecs/features/warn_on.prf \
		/usr/local/Qt4.7/mkspecs/features/qt.prf \
		/usr/local/Qt4.7/mkspecs/features/unix/thread.prf \
		/usr/local/Qt4.7/mkspecs/features/moc.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/rez.prf \
		/usr/local/Qt4.7/mkspecs/features/mac/sdk.prf \
		/usr/local/Qt4.7/mkspecs/features/resources.prf \
		/usr/local/Qt4.7/mkspecs/features/uic.prf \
		/usr/local/Qt4.7/mkspecs/features/yacc.prf \
		/usr/local/Qt4.7/mkspecs/features/lex.prf \
		/usr/local/Qt4.7/mkspecs/features/include_source_dir.prf \
		/Library/Frameworks/QtGui.framework/QtGui.prl \
		/Library/Frameworks/QtCore.framework/QtCore.prl
	$(QMAKE) -spec /usr/local/Qt4.7/mkspecs/macx-g++ CONFIG+=x86_64 -o Makefile JungleAdventure.pro
/usr/local/Qt4.7/mkspecs/common/unix.conf:
/usr/local/Qt4.7/mkspecs/common/mac.conf:
/usr/local/Qt4.7/mkspecs/common/mac-g++.conf:
/usr/local/Qt4.7/mkspecs/qconfig.pri:
/usr/local/Qt4.7/mkspecs/modules/qt_webkit_version.pri:
/usr/local/Qt4.7/mkspecs/features/qt_functions.prf:
/usr/local/Qt4.7/mkspecs/features/qt_config.prf:
/usr/local/Qt4.7/mkspecs/features/exclusive_builds.prf:
/usr/local/Qt4.7/mkspecs/features/default_pre.prf:
/usr/local/Qt4.7/mkspecs/features/mac/default_pre.prf:
/usr/local/Qt4.7/mkspecs/features/mac/dwarf2.prf:
/usr/local/Qt4.7/mkspecs/features/debug.prf:
/usr/local/Qt4.7/mkspecs/features/default_post.prf:
/usr/local/Qt4.7/mkspecs/features/mac/default_post.prf:
/usr/local/Qt4.7/mkspecs/features/mac/objective_c.prf:
/usr/local/Qt4.7/mkspecs/features/mac/x86_64.prf:
/usr/local/Qt4.7/mkspecs/features/warn_on.prf:
/usr/local/Qt4.7/mkspecs/features/qt.prf:
/usr/local/Qt4.7/mkspecs/features/unix/thread.prf:
/usr/local/Qt4.7/mkspecs/features/moc.prf:
/usr/local/Qt4.7/mkspecs/features/mac/rez.prf:
/usr/local/Qt4.7/mkspecs/features/mac/sdk.prf:
/usr/local/Qt4.7/mkspecs/features/resources.prf:
/usr/local/Qt4.7/mkspecs/features/uic.prf:
/usr/local/Qt4.7/mkspecs/features/yacc.prf:
/usr/local/Qt4.7/mkspecs/features/lex.prf:
/usr/local/Qt4.7/mkspecs/features/include_source_dir.prf:
/Library/Frameworks/QtGui.framework/QtGui.prl:
/Library/Frameworks/QtCore.framework/QtCore.prl:
qmake:  FORCE
	@$(QMAKE) -spec /usr/local/Qt4.7/mkspecs/macx-g++ CONFIG+=x86_64 -o Makefile JungleAdventure.pro

JungleAdventure.app/Contents/PkgInfo: 
	@$(CHK_DIR_EXISTS) JungleAdventure.app/Contents || $(MKDIR) JungleAdventure.app/Contents 
	@$(DEL_FILE) JungleAdventure.app/Contents/PkgInfo
	@echo "APPL????" >JungleAdventure.app/Contents/PkgInfo
JungleAdventure.app/Contents/Resources/empty.lproj: 
	@$(CHK_DIR_EXISTS) JungleAdventure.app/Contents/Resources || $(MKDIR) JungleAdventure.app/Contents/Resources 
	@touch JungleAdventure.app/Contents/Resources/empty.lproj
	
JungleAdventure.app/Contents/Info.plist: 
	@$(CHK_DIR_EXISTS) JungleAdventure.app/Contents || $(MKDIR) JungleAdventure.app/Contents 
	@$(DEL_FILE) JungleAdventure.app/Contents/Info.plist
	@sed -e "s,@ICON@,,g" -e "s,@EXECUTABLE@,JungleAdventure,g" -e "s,@TYPEINFO@,????,g" /usr/local/Qt4.7/mkspecs/macx-g++/Info.plist.app >JungleAdventure.app/Contents/Info.plist
dist: 
	@$(CHK_DIR_EXISTS) .tmp/JungleAdventure1.0.0 || $(MKDIR) .tmp/JungleAdventure1.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) .tmp/JungleAdventure1.0.0/ && $(COPY_FILE) --parents player.h game.h bullet.h enemy.h boss.h fire.h health.h healthpackage.h ammo.h ammo_package.h .tmp/JungleAdventure1.0.0/ && $(COPY_FILE) --parents res.qrc .tmp/JungleAdventure1.0.0/ && $(COPY_FILE) --parents main.cpp player.cpp game.cpp bullet.cpp enemy.cpp boss.cpp fire.cpp health.cpp healthpackage.cpp ammo.cpp ammo_package.cpp .tmp/JungleAdventure1.0.0/ && (cd `dirname .tmp/JungleAdventure1.0.0` && $(TAR) JungleAdventure1.0.0.tar JungleAdventure1.0.0 && $(COMPRESS) JungleAdventure1.0.0.tar) && $(MOVE) `dirname .tmp/JungleAdventure1.0.0`/JungleAdventure1.0.0.tar.gz . && $(DEL_FILE) -r .tmp/JungleAdventure1.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) -r JungleAdventure.app
	-$(DEL_FILE) Makefile


check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_objective_c_make_all:
compiler_objective_c_clean:
compiler_moc_header_make_all: moc_player.cpp moc_game.cpp moc_bullet.cpp moc_enemy.cpp moc_boss.cpp moc_fire.cpp moc_health.cpp moc_ammo.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_player.cpp moc_game.cpp moc_bullet.cpp moc_enemy.cpp moc_boss.cpp moc_fire.cpp moc_health.cpp moc_ammo.cpp
moc_player.cpp: player.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ player.h -o moc_player.cpp

moc_game.cpp: bullet.h \
		player.h \
		health.h \
		boss.h \
		ammo.h \
		game.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ game.h -o moc_game.cpp

moc_bullet.cpp: bullet.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ bullet.h -o moc_bullet.cpp

moc_enemy.cpp: enemy.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ enemy.h -o moc_enemy.cpp

moc_boss.cpp: boss.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ boss.h -o moc_boss.cpp

moc_fire.cpp: fire.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ fire.h -o moc_fire.cpp

moc_health.cpp: health.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ health.h -o moc_health.cpp

moc_ammo.cpp: ammo.h
	/Developer/Tools/Qt/moc $(DEFINES) $(INCPATH) -D__APPLE__ -D__GNUC__ ammo.h -o moc_ammo.cpp

compiler_rcc_make_all: qrc_res.cpp
compiler_rcc_clean:
	-$(DEL_FILE) qrc_res.cpp
qrc_res.cpp: res.qrc \
		jungle.jpg \
		ghost.png \
		health_package.png \
		ammo_package.png \
		player.png \
		boss.png \
		jungle.png \
		boss_fire.png \
		bullet.png
	/Developer/Tools/Qt/rcc -name res res.qrc -o qrc_res.cpp

compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_rez_source_make_all:
compiler_rez_source_clean:
compiler_uic_make_all:
compiler_uic_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_header_clean compiler_rcc_clean 

####### Compile

main.o: main.cpp game.h \
		bullet.h \
		player.h \
		health.h \
		boss.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

player.o: player.cpp player.h \
		bullet.h \
		game.h \
		health.h \
		boss.h \
		ammo.h \
		healthpackage.h \
		enemy.h \
		ammo_package.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o player.o player.cpp

game.o: game.cpp game.h \
		bullet.h \
		player.h \
		health.h \
		boss.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o game.o game.cpp

bullet.o: bullet.cpp bullet.h \
		enemy.h \
		boss.h \
		game.h \
		player.h \
		health.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o bullet.o bullet.cpp

enemy.o: enemy.cpp enemy.h \
		player.h \
		game.h \
		bullet.h \
		health.h \
		boss.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o enemy.o enemy.cpp

boss.o: boss.cpp boss.h \
		fire.h \
		game.h \
		bullet.h \
		player.h \
		health.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o boss.o boss.cpp

fire.o: fire.cpp fire.h \
		bullet.h \
		enemy.h \
		boss.h \
		game.h \
		player.h \
		health.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o fire.o fire.cpp

health.o: health.cpp health.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o health.o health.cpp

healthpackage.o: healthpackage.cpp enemy.h \
		healthpackage.h \
		player.h \
		game.h \
		bullet.h \
		health.h \
		boss.h \
		ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o healthpackage.o healthpackage.cpp

ammo.o: ammo.cpp ammo.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ammo.o ammo.cpp

ammo_package.o: ammo_package.cpp enemy.h \
		healthpackage.h \
		player.h \
		game.h \
		bullet.h \
		health.h \
		boss.h \
		ammo.h \
		ammo_package.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ammo_package.o ammo_package.cpp

moc_player.o: moc_player.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_player.o moc_player.cpp

moc_game.o: moc_game.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_game.o moc_game.cpp

moc_bullet.o: moc_bullet.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_bullet.o moc_bullet.cpp

moc_enemy.o: moc_enemy.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_enemy.o moc_enemy.cpp

moc_boss.o: moc_boss.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_boss.o moc_boss.cpp

moc_fire.o: moc_fire.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_fire.o moc_fire.cpp

moc_health.o: moc_health.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_health.o moc_health.cpp

moc_ammo.o: moc_ammo.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_ammo.o moc_ammo.cpp

qrc_res.o: qrc_res.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o qrc_res.o qrc_res.cpp

####### Install

install:   FORCE

uninstall:   FORCE

FORCE:

